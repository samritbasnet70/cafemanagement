var express = require('express');
var router = express.Router();
const fs=require('fs');

var UserModel = require('../models/usermodel.js')

function createUser(req, res, next) {
    const user=new UserModel(req.body);

    if(req.file){
        user.image=req.file.filename;
    }
    user.save((err,result)=>{
        if(err){
            res.json(err);
        }else{
            res.json(result);
        }
    })

    function findUser(req, res, next) {
        UserModel.find({},(err,result)=>{
            if(err){
                res.json(err);
            }else{
                res.json(result);
            }
        })
    }

    function updateUser(req, res, next) {
        UserModel.updateOne({
            _id:req.params.id
        },
        {
            $set:req.body
        },
        {
            upsert:true
        })
        .then(result=>{
            res.json(req.body);
        })
        .catch(err=>{
            res.json(err)
        })
        function deleteUser(req, res, next) {
            UserModel.deleteOne({
                _id: req.params.id
            })
            .then(result => {
                res.json(result);
            })
            .catch(err => {
                res.json(err);
            })
        }
        
    
    }
}
module.exports = {
    createUser,
    findUser,
    updateUser,
    deleteUser}