var express = require('express');
var router = express.Router();
const upload=require('../upload')

const {
  createUser,
  findUser,
  updateUser,
  deleteUser
}=require('../controller/user');

router.route('/')
.get(findUser)
.post(upload.single('image'),createUser);

router.route('/:id')
.put(updateUser)
.delete(deleteUser)

module.exports = router;
