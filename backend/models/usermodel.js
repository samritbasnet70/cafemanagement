const mongoose = require('mongoose')
const userSchema= new mongoose.Schema({
    firstname:{
        type: 'string',
        require:true
    },
    lastname:{
        type: String,
        require:true
    },
    email:{
        type: String,
        require:true,
        unique:true
    },
    username: {
        type: String,
        require:true,
        unique:true
    },
    password: {
        type: String,
        require:true,
    },
    phone: {
        type:Number,
        require:true
    },
    gender: {
        type: String,
    },
    image:{
        type: String,
    },
    role: {
        type: String,
        default: 'user',
        enum:["basic",'cafemanager','admin']

    }
})

const User = mongoose.model('User',userSchema);
module.exports =User;